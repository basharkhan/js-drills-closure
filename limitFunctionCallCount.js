function limitFunctionCallCount(callback, n){

    for(let index = 0; index < n; index++){
        let value = index + 4;
        callback(value);
    }
    
}

module.exports = limitFunctionCallCount;