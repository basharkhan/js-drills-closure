function counterFactory() {

    let current = 1;

    var increment = function () {
        return console.log(current++);
    }

    var decrement = function () {
        return console.log(current--);
    }

    return {
        increment: increment,
        decrement: decrement
    };



}

let result = counterFactory();


module.exports = result;
